<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="alert" style="background-color: #fafafa;">

    <div class="alert alert-info">
        <h4>Insert your data</h4>
    </div>
    <c:url var="personUrl" value='/person'/>
    <form:form modelAttribute="person" method="POST" action="${personUrl}">
        <table>
            <tr>
                <td><form:label path="firstName">First Name:</form:label></td>
                <td><form:input path="firstName"/></td>
            </tr>

            <tr>
                <td><form:label path="lastName">Last Name</form:label></td>
                <td><form:input path="lastName"/></td>
            </tr>

            <tr>
                <td><form:label path="address">Address</form:label></td>
                <td><form:input path="address"/></td>
            </tr>
        </table>

        <input class="btn" type="reset" value="Reset"/>
        <input class="btn btn-primary" type="submit" value="Submit"/>
    </form:form>

</div>
