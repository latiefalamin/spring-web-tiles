<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="hero-unit">
    <h1>Mari Belajar Java Web Programming!</h1>

    <p>Kita belajar membuat Web dengan Java+Spring Framework.</p>
    <a href="<c:url value='/person/form'/>" class="btn btn-primary btn-large">Form Person</a>
</div>
