<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="hero-unit">
    <h1>Sencha Ext JS </h1>

    <p>Sencha Ext JS is the leading standard for business-grade web application development. With over 100 examples,
        1000 APIs, hundreds of components, a full documentation suite and built in themes, Ext JS provides the tools
        necessary to build robust desktop applications.</p>
</div>
