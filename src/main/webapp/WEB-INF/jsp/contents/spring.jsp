<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<!-- Main hero unit for a primary marketing message or call to action -->
<div class="hero-unit">
    <h1>What is Spring?</h1>

    <p>Spring is the most popular application development framework for enterprise Java™. Millions of developers use Spring to create high performing, easily testable, reusable code without any lock-in.</p>
</div>

