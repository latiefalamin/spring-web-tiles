<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="hero-unit">
    <h1>Java Persistence API</h1>

    <p>The Java Persistence API provides a POJO persistence model for object-relational mapping. The Java Persistence
        API was developed by the EJB 3.0 software expert group as part of JSR 220, but its use is not limited to EJB
        software components. It can also be used directly by web applications and application clients, and even outside
        the Java EE platform, for example, in Java SE applications. See JSR 220.</p>
</div>
