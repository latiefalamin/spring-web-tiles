<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="hero-unit">
    <h1>Hibernate!</h1>

    <p>Hibernate takes care of the mapping from Java classes to database tables, and from Java data types to SQL data
        types. In addition, it provides data query and retrieval facilities. It can significantly reduce development
        time otherwise spent with manual data handling in SQL and JDBC. Hibernate’s design goal is to relieve the
        developer from 95% of common data persistence-related programming tasks by eliminating the need for manual,
        hand-crafted data processing using SQL and JDBC. However, unlike many other persistence solutions, Hibernate
        does not hide the power of SQL from you and guarantees that your investment in relational technology and
        knowledge is as valid as always.</p>
</div>
