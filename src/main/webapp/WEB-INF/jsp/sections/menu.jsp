<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <ul class="nav">
                <li class="active"><a href="<c:url value='/'/>">Home</a></li>
                <li><a href="<c:url value='/spring'/>">Spring</a></li>
                <li><a href="<c:url value='/jpa'/>">JPA</a></li>
                <li><a href="<c:url value='/extjs'/>">Extjs</a></li>
                <li><a href="<c:url value='/hibernate'/>">Hibernate</a></li>
            </ul>
        </div>
    </div>
</div>